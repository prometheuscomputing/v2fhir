<?xml version="1.0" encoding="utf-8"?>
<DataTypeDefinition>
  <url value="http://v2.hl7.org/fhir/DataTypeDefinition/time"/>
  <name value="time"/>
  <withdrawn value=""/>
  <abbreviation value="TM"/>
  <description>
    <status value="additional"/>
    <div>
      <div class="insert-component-table" id="TM-component-table" style="display: none;"/>
      <p class="v2 v2-normalindented"><strong>Definition:</strong> Specifies the hour of the day with optional minutes, seconds, fraction of second using a 24-hour clock notation and time zone.</p>
      <p class="v2 v2-normalindented">As of v 2.3, the number of characters populated (excluding the time zone specification) specifies the precision. </p>
      <p class="v2 v2-normalindented">Format: HH[MM[SS[.S[S[S[S]]]]]][+/-ZZZZ]</p>
      <p class="v2 v2-normalindented">Thus: </p>
      <ol class="v2 v2-list v2-lower-alpha-list v2-list" style="list-style: lower-alpha">
        <li class="v2 v2-li">
          <p class="v2 v2-list">the first two are used to specify a precision of "hour”</p>
        </li>
        <li class="v2 v2-li">
          <p class="v2 v2-list">the first four are used to specify a precision of "minute”</p>
        </li>
        <li class="v2 v2-li">
          <p class="v2 v2-list">the first six are used to specify a precision of "second”</p>
        </li>
        <li class="v2 v2-li">
          <p class="v2 v2-list">the first eight are used to specify a precision of "one tenth of a second”</p>
        </li>
        <li class="v2 v2-li">
          <p class="v2 v2-list">the first eleven are used to specify a precision of " one ten thousandths of a second”</p>
        </li>
      </ol>
      <p class="v2 v2-normalindented">Example: |0630| specifies 6: 30 AM.</p>
      <p class="v2 v2-normalindented">The fractional seconds could be sent by a transmitter who requires greater precision than whole seconds. Fractional representations of minutes, hours or other higher-order units of time are not permitted.</p>
      <div class="v2-note">
        <p class="v2"><strong>Note:</strong> The time zone [+/-ZZZZ], when used, is restricted to legally-defined time zones and is represented in HHMM format.</p>
      </div>
      <br/>
      <p class="v2 v2-normalindented">The time zone of the sender may be sent optionally as an offset from the coordinated universal time (previously known as Greenwich Mean Time). Where the time zone is not present in a particular TM field but is included as part of the date/time field in the MSH segment, the MSH value will be used as the default time zone. Otherwise, the time is understood to refer to the local time of the sender.</p>
      <p class="v2 v2-normalindented">Examples:</p>
      <table class="v2" style="width: 76%; border-top: 1px solid black; border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; ">
        <colgroup>
          <col span="1" style="width: 19.94%;"/>
          <col span="1" style="width: 80.05%;"/>
        </colgroup>
        <tr>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; background-color: #E6E6E6; width:19.94%; ">
            <p class="v2 v2-othertableheader">Time </p>
          </td>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; background-color: #E6E6E6; width:80.05%; ">
            <p class="v2 v2-othertableheader">Description</p>
          </td>
        </tr>
        <tr>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; width:19.94%; ">
            <p class="v2 v2-othertablebody">|0000|</p>
          </td>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; width:80.05%; ">
            <p class="v2 v2-othertablebody">midnight</p>
          </td>
        </tr>
        <tr>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; width:19.94%; ">
            <p class="v2 v2-othertablebody">|235959+1100|</p>
          </td>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; width:80.05%; ">
            <p class="v2 v2-othertablebody">1 second before midnight in a time zone eleven hours ahead of Universal Coordinated Time (i.e., East of Greenwich).</p>
          </td>
        </tr>
        <tr>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; width:19.94%; ">
            <p class="v2 v2-othertablebody">|0800|</p>
          </td>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; width:80.05%; ">
            <p class="v2 v2-othertablebody">Eight AM, local time of the sender.</p>
          </td>
        </tr>
        <tr>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; width:19.94%; ">
            <p class="v2 v2-othertablebody">|093544.2312|</p>
          </td>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; width:80.05%; ">
            <p class="v2 v2-othertablebody">44.2312 seconds after Nine thirty-five AM, local time of sender.</p>
          </td>
        </tr>
        <tr>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; width:19.94%; ">
            <p class="v2 v2-othertablebody">|13|</p>
          </td>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; width:80.05%; ">
            <p class="v2 v2-othertablebody">1pm (with a precision of hours), local time of sender.</p>
          </td>
        </tr>
      </table>
      <br/>
      <p class="v2 v2-normalindented">Prior to v 2.3, this data type was specified in the format HHMM[SS[.SSSS]][+/-ZZZZ]. As of v 2.3 minutes are no longer required. By site-specific agreement, HHMM[SS[.SSSS]][+/-ZZZZ] may be used where backward compatibility must be maintained.This corresponds a minimum length of 4.</p>
      <p class="v2 v2-normalindented">The TM data type does not follow the normal truncation pattern, and the truncation character is never valid in the TM data type. Instead, the truncation behavior is based on the semantics of times. </p>
      <p class="v2 v2-normalindented">Unless otherwise specified in the context where the DTM type is used, the DTM type may be truncated to a particular minute. When a TM is truncated, the truncated form SHALL still be a valid TM type. Refer to Chapter 2, section 2.5.5.2, "Truncation Pattern", for further information.</p>
    </div>
  </description>
  <component>
    <name value="Time"/>
    <sequenceNumber value=""/>
    <withdrawn value="false"/>
    <minLength value="2"/>
    <maxLength value="16"/>
    <confLength value="13"/>
    <mayTruncate value="true"/>
    <dataType value="http://v2.hl7.org/fhir/DataTypeDefinition/time"/>
  </component>
</DataTypeDefinition>
