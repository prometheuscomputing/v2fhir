<?xml version="1.0" encoding="utf-8"?>
<SegmentDefinition>
  <url value="http://v2.hl7.org/fhir/SegmentDefinition/participation-information-segment"/>
  <name value="Participation Information Segment"/>
  <abbreviation value="PRT"/>
  <withdrawn value=""/>
  <description>
    <status value="additional"/>
    <div>
      <p class="v2 v2-normalindented">The Participation Information segment contains the data necessary to add, update, correct, and delete from the record persons, organizations, devices, or locations (participants) participating in the activity being transmitted.</p>
      <p class="v2 v2-normalindented">In general, the PRT segment is used to describe a participant playing a particular role within the context of the message.  In OO, for example, in the results messages the PRT segment may be used to provide the performing provider, whether a person or organization.  In a specimen shipment message it may be the waypoint location relevant for the shipment.</p>
      <p class="v2 v2-normalindented">The positional location of the PRT segment indicates the relationship.  When the segment is used following the OBX segment, then the participations relate to that OBX addressing participations such as responsible observer.</p>
      <p class="v2 v2-normalindented">The PRT segment may be used to communicate U.S. FDA Unique Device Identifier (UDI<span id="chapter-7-footnote-reference-2"><a href="#chapter-7-footnote-2"><sup>2</sup></a></span>) information, with the PRT-10 field containing the UDI and additional fields added to contain UDI elements, when it is advised to communicate these individually (see Guidance in PRT-10 definition).  These identifiers are intended to cover a wide variety of devices.  When representing a UDI, PRT-4 would be “EQUIP”.</p>
      <div class="insert-segment-table" id="PRT-attribute-table" style="display: none;"/>
    </div>
  </description>
  <field>
    <sequenceNumber value="1"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <flags value="C"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/02379"/>
  </field>
  <field>
    <sequenceNumber value="2"/>
    <mustSupport value="true"/>
    <minCardinality value="1"/>
    <maxCardinality value="1"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/00816"/>
  </field>
  <field>
    <sequenceNumber value="3"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/02380"/>
  </field>
  <field>
    <sequenceNumber value="4"/>
    <mustSupport value="true"/>
    <minCardinality value="1"/>
    <maxCardinality value="1"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/02381"/>
  </field>
  <field>
    <sequenceNumber value="5"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <flags value="C"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/02382"/>
  </field>
  <field>
    <sequenceNumber value="6"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <flags value="C"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/02383"/>
  </field>
  <field>
    <sequenceNumber value="7"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <flags value="C"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/02384"/>
  </field>
  <field>
    <sequenceNumber value="8"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <flags value="C"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/02385"/>
  </field>
  <field>
    <sequenceNumber value="9"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <flags value="C"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/02386"/>
  </field>
  <field>
    <sequenceNumber value="10"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <flags value="C"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/02348"/>
  </field>
  <field>
    <sequenceNumber value="11"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/02387"/>
  </field>
  <field>
    <sequenceNumber value="12"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/02388"/>
  </field>
  <field>
    <sequenceNumber value="13"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/02389"/>
  </field>
  <field>
    <sequenceNumber value="14"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <flags value="C"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/02390"/>
  </field>
  <field>
    <sequenceNumber value="15"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/02391"/>
  </field>
  <field>
    <sequenceNumber value="16"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/03476"/>
  </field>
  <field>
    <sequenceNumber value="17"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/03477"/>
  </field>
  <field>
    <sequenceNumber value="18"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/03478"/>
  </field>
  <field>
    <sequenceNumber value="19"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/03479"/>
  </field>
  <field>
    <sequenceNumber value="20"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/03480"/>
  </field>
  <field>
    <sequenceNumber value="21"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/03481"/>
  </field>
  <field>
    <sequenceNumber value="22"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <flags value="C"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/03483"/>
  </field>
  <field>
    <sequenceNumber value="23"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <maxCardinality value="1"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/00684"/>
  </field>
  <field>
    <sequenceNumber value="24"/>
    <mustSupport value="false"/>
    <minCardinality value="0"/>
    <dataElement value="http://v2.hl7.org/fhir/DataElementDefinition/01171"/>
  </field>
</SegmentDefinition>
