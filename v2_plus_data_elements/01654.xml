<?xml version="1.0" encoding="utf-8"?>
<DataElementDefinition>
  <url value="http://v2.hl7.org/fhir/DataElementDefinition/01654"/>
  <name value="Cyclic Entry/Exit Indicator"/>
  <description>
    <status value="additional"/>
    <div>
      <p class="v2 v2-normalindented">Definition:  Indicates if this service request is the first, last, service request in a cyclic series of service requests. If null or not present, this field indicates that the current service request is neither the first or last service request in a cyclic series of service requests.  Refer to <a href="https://www.hl7.org/fhir/v2/0505/index.html">HL7 Table 0505 – Cyclic Entry/Exit Indicator</a> in Chapter 2C, Code Tables, for allowed values.</p>
      <p class="v2 v2-normalindented">Conditional Rule: Should not be populated when TQ2-2 (Sequence/Results Flag) is not equal to a 'C' (cyclic service request).</p>
      <p class="v2 v2-normalindented">Example of TQ2 - 6, 7, &amp; 8 Usage:</p>
      <table class="v2" style="width: 86%; border-top: 1px solid black; border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; ">
        <colgroup>
          <col span="1" style="width: 23.54%;"/>
          <col span="1" style="width: 76.45%;"/>
        </colgroup>
        <tr>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; background-color: #E6E6E6; width:23.54%; ">
            <p class="v2 v2-othertableheader">Example</p>
          </td>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; background-color: #E6E6E6; width:76.45%; ">
            <p class="v2 v2-othertableheader">Translation</p>
          </td>
        </tr>
        <tr>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; width:23.54%; ">
            <p class="v2 centered v2-othertablebody">...|ES|*|+10^min|...</p>
          </td>
          <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; width:76.45%; ">
            <p class="v2 v2-othertablebody">translates to: execute this service request the first time without evaluating the condition specified in the TQ2 segment; but repeat only its execution when the specified external service request's start or finish date/time has met this condition. This specification generates a repetition of the service request for each iteration of the cycle.</p>
          </td>
        </tr>
      </table>
      <br/>
      <div class="v2-note">
        <p class="v2"><strong>Note: </strong>This requires that the requesting application be able to specify the placer/filler/placer group number of the last service request in the cycle in the first service request's quantity/timing specification.</p>
      </div>
      <br/>
    </div>
  </description>
  <itemNumber value="1654"/>
  <minLength value="1"/>
  <maxLength value="1"/>
  <confLength value=""/>
  <mayTruncate value="not applicable"/>
  <table value="https://www.hl7.org/fhir/v2/0505/index.html"/>
  <dataType value="http://v2.hl7.org/fhir/DataTypeDefinition/coded-value-for-hl7-defined-tables"/>
</DataElementDefinition>
