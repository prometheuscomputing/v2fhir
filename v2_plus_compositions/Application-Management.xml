<?xml version="1.0" encoding="utf-8"?>
<Composition xmlns="http://hl7.org/fhir">
  <url value="http://v2.hl7.org/fhir/Composition/Application-Management"/>
  <status value="preliminary"/>
  <type>
    <text value="HL7 V2+ Standards Document"/>
  </type>
  <date value="2020-12-09"/>
  <title value="Application Management"/>
  <custodian>
    <reference value="uri/for/an/instance/of/Organization/that/is/the/HL7/organization"/>
  </custodian>
  <section>
    <title value="Frontmatter for Application Management"/>
    <code>
      <text value="pre"/>
    </code>
    <text>
      <status value="additional"/>
      <div>
        <p class="v2"/>
        <table class="v2" style="width: 100%; ">
          <colgroup>
            <col span="1" style="width: 35.93%;"/>
            <col span="1" style="width: 64.06%;"/>
          </colgroup>
          <tr>
            <td style="width:35.93%; ">
              <p class="v2">Chapter Co-Chair and Editor</p>
            </td>
            <td style="width:64.06%; ">
              <p class="v2">Anthony Julian<br/>Mayo Clinic</p>
            </td>
          </tr>
          <tr>
            <td style="width:35.93%; ">
              <p class="v2">Chapter Co-Chair</p>
            </td>
            <td style="width:64.06%; ">
              <p class="v2">Nick Radov</p>
              <p class="v2">United Healthcare</p>
            </td>
          </tr>
          <tr>
            <td style="width:35.93%; ">
              <p class="v2">Chapter Co-Chair </p>
            </td>
            <td style="width:64.06%; ">
              <p class="v2">Sandra Stuart<br/>Kaiser Permanente</p>
            </td>
          </tr>
          <tr>
            <td style="width:35.93%; ">
              <p class="v2">Chapter Co-Chair</p>
            </td>
            <td style="width:64.06%; ">
              <p class="v2">Dave Shaver<br/>Corepoint Health</p>
            </td>
          </tr>
          <tr>
            <td style="width:35.93%; ">
              <p class="v2">Sponsoring Work Group:</p>
            </td>
            <td style="width:64.06%; ">
              <p class="v2">Infrastructure and Messaging</p>
            </td>
          </tr>
          <tr>
            <td style="width:35.93%; ">
              <p class="v2">List Server:</p>
            </td>
            <td style="width:64.06%; ">
              <p class="v2">
                <a href="inm@lists.hl7.org">inm@lists.hl7.org</a>
              </p>
            </td>
          </tr>
        </table>
        <br/>
        <p class="v2"/>
        <p class="v2"/>
      </div>
    </text>
  </section>
  <section>
    <title value="Introduction"/>
    <code>
      <text value="clause"/>
    </code>
    <text>
      <status value="additional"/>
      <div>
        <p class="v2">The information in this chapter was relocated from Appendix C as of v2.4 of the standard.  It had previously been entitled Network Management, and has been renamed to more accurately describe the purpose of the messages described herein.  This chapter does not specify a protocol for managing networks, á la TCP/IP SNMP.  Rather, its messages provide a means to manage HL7-supporting applications over a network. </p>
        <p class="v2">Because this chapter was originally named "Network Management," the messages and segments have labels beginning with the letter "N."  These labels are retained for backward compatibility.</p>
        <p class="v2">As a technical chapter, this information is now normative with respect to the HL7 standard.  It is anticipated that additional messages and message content will be added to this chapter in the near future.  </p>
      </div>
    </text>
  </section>
  <section>
    <title value="Trigger Events and Message Definitions"/>
    <code>
      <text value="clause"/>
    </code>
    <section>
      <title value="NMQ - Application Management Query Message (Event N01)"/>
      <code>
        <text value="clause"/>
      </code>
      <text>
        <status value="additional"/>
        <div>
          <p class="v2 v2-normalindented"><strong>NOTE: The MFQ//MFR transaction was retained for backward compatibility as of v2.5 and has been withdrawn as of V2.7.</strong>  See conformance based queries as defined in Chapter 5.</p>
        </div>
      </text>
      <section>
        <title value="Acknowledgement Choreography"/>
        <code>
          <text value="clause"/>
        </code>
        <text>
          <status value="additional"/>
          <div>
            <p class="v2 v2-normalindented">None - Refer to Chapter 5 for generic query choreography.</p>
          </div>
        </text>
      </section>
    </section>
    <section>
      <title value="NMD - Application Management Data Message (Event N02)"/>
      <code>
        <text value="clause"/>
      </code>
      <text>
        <status value="additional"/>
        <div>
          <p class="v2 v2-normalindented">The N02 event signifies when an unsolicited update (UU) Application Management Data message (NMD) is created by on application to transmit application management information to other applications.  In this case, the initiating application sends an NMD message as an unsolicited update (UU) containing application management information to a receiving application, which responds with a generic acknowledgement message (ACK).</p>
          <p class="v2 v2-normalindented">For example, an application going down for backups (or starting up again after backups) might issue such a message to one or more other applications.  An application switching to another CPU or file-server may also need to use this transaction to notify other systems.</p>
          <div class="insert-msg-table" id="NMD^N02^NMD_N02: Application Management Data-message-table" style="display: none;">NMD^N02^NMD_N02: Application Management Data Table</div>
          <p class="v2 v2-endnotetext"/>
          <p class="v2"/>
          <div class="v2-note">
            <p class="v2">Note:  Because this message pair does not have an application acknowledgement, the value in MSH-16 does not affect the choreography</p>
          </div>
          <br/>
          <p class="v2 centered"/>
          <div class="insert-msg-table" id="ACK^N02^ACK: Generic Acknowledgement-message-table" style="display: none;">ACK^N02^ACK: Generic Acknowledgement Table</div>
          <p class="v2"/>
          <p class="v2"/>
        </div>
      </text>
    </section>
  </section>
  <section>
    <title value="message segments"/>
    <code>
      <text value="clause"/>
    </code>
    <section>
      <title value="NCK - System Clock Segment"/>
      <code>
        <text value="segment-defintion"/>
      </code>
      <entry>
        <reference value="http://v2.hl7.org/fhir/SegmentDefinition/system-clock-segment"/>
      </entry>
    </section>
    <section>
      <title value="NSC – Application Status Change Segment"/>
      <code>
        <text value="segment-defintion"/>
      </code>
      <entry>
        <reference value="http://v2.hl7.org/fhir/SegmentDefinition/application-status-change-segment"/>
      </entry>
    </section>
    <section>
      <title value="NST – Application Control-Level Statistics Segment"/>
      <code>
        <text value="segment-defintion"/>
      </code>
      <entry>
        <reference value="http://v2.hl7.org/fhir/SegmentDefinition/application-control-level-statistics-segment"/>
      </entry>
    </section>
  </section>
  <section>
    <title value="outstanding issues"/>
    <code>
      <text value="clause"/>
    </code>
    <text>
      <status value="additional"/>
      <div>
        <p class="v2">None.</p>
        <p class="v2"/>
        <p class="v2"/>
        <p class="v2"/>
      </div>
    </text>
  </section>
</Composition>
